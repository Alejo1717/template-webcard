module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'sans': ['Roboto', 'Helvetica', 'Arial', 'sans-serif']
      }
    },
    colors: {
      primary: '#2D455D',
      secondary: '#E1AC9E',
      third: '#85BAE3',
      fourth: '#39A9DD',
      fifth: '#2D455D',
      inputcolor: '#EAE5E5B0',
      whitecolor: '#FFFF',
      blackcolor: '#000',
      darkgray: '#707070',
      gray: '#5C5C5C',
      bluecolor:'#2C4491',
      darkblue: '#233342',
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}